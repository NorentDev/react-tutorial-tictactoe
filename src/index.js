import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Winner {
  static squares; //Array(9)
  static winSquares = Array(3).fill(null);

  static indexesOfWinLines = [
    [0, 1, 2], //tip: this is the reference layout
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6], //tip: make use here of the above reference layout
    [1, 4, 7], //
    [2, 5, 8], //
    [0, 4, 8], //
    [2, 4, 6], //
  ];

  static find = (squares) => {
    this.squares = squares;
    return this.indexesOfWinLines.reduce(this.findReducedWinnerOfLine, null);
  }

  static setWinSquares = () => {
    this.winSquares = this.indexesOfWinLines.reduce(this.findReducedWinSquaresOfLine, null);
  }

  static findReducedWinnerOfLine = (foundWinner, currentLine) => {
    if (foundWinner) {
      return foundWinner;
    } else {
      return this.findWinnerOfCurrentLine(currentLine);
    }
  }

  static findReducedWinSquaresOfLine = (foundSquares, currentLine) => {
    if (foundSquares) {
      return foundSquares;
    } else {
      return this.findWinSquaresofCurrentLine(currentLine);
    }
  }

  static findWinnerOfCurrentLine = (currentLine) => {
    const isSquareOfCurrentLine = (...[, index]) => currentLine.includes(index);
    const isEqualToFirstValue = (...[value, , array]) => value === array[0];

    const playersOfCurrentLine = this.squares.filter(isSquareOfCurrentLine);
    const currentLineWins = playersOfCurrentLine.every(isEqualToFirstValue);

    return currentLineWins
      ? playersOfCurrentLine[0]
      : null;
  }

  static findWinSquaresofCurrentLine = (currentLine) => {
    const winner = this.findWinnerOfCurrentLine(currentLine);

    return winner
      ? currentLine
      : null;
  }
}

function Square(props) {
  const display = props.win
    ? '*' + props.value + '*'
    : props.value;
  return (
    <button
      className="square"
      onClick={props.onClick}
    >
      {display}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    const winSquare = Winner.winSquares.includes(i);

    return (
      <Square
        key={i}
        value={this.props.squares[i]}
        onClick={() => { this.props.onClick(i) }}
        win={winSquare}
      />
    );
  }

  renderBoard() {
    const board = [];
    let row = [];
    const numberRows = 3;
    const numberColumns = 3;

    let indexSquare = 0;
    for (let i = 0; i < numberRows; i++) {
      row = [];
      for (let j = 0; j < numberColumns; j++) {
        row.push(
          this.renderSquare(indexSquare++));
      }
      board.push(<div className="board-row" key={i}>{row}</div>);
    }

    return board;
  }


  render() {
    return (
      <div>
        {this.renderBoard()}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        lastMove: {
          column: null,
          row: null,
          x: null,
          stepNumber: 0,
        }
      }],
      stepNumber: 0,
      xIsNext: true,
      orderAscending: true,
    };
  }

  getRow(i) {
    const row = Math.floor(i / 3);
    return row;
  }

  getColumn(i) {
    const column = i % 3;
    return column;
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    const winner = Winner.find(squares);
    const alreadyMarked = Boolean(squares[i]);
    const noAction = winner || alreadyMarked;
    if (noAction) {
      return;
    }

    squares[i] = this.state.xIsNext ? 'X' : 'O';

    const newState = {
      history: history.concat([{
        squares: squares,
        lastMove: {
          column: this.getColumn(i),
          row: this.getRow(i),
          x: this.state.xIsNext,
          stepNumber: history.length,
        }
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    };

    this.setState(newState);
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }

  toggleOrder() {
    const ascendingOrder = this.state.ascendingOrder;
    this.setState({
      ascendingOrder: !ascendingOrder,
    })
  }

  render() {
    const state = this.state;
    const unsortedHistory = state.history;
    const current = unsortedHistory[state.stepNumber];
    const winner = Winner.find(current.squares);
    const draw = !current.squares.includes(null);
    const sortedHistory = state.ascendingOrder
      ? unsortedHistory
      : unsortedHistory.slice().reverse();

    if (winner) {
      Winner.setWinSquares();
    }

    const toggleOrderButtonText = this.state.ascendingOrder
      ? 'sort descending'
      : 'sort ascending';
    const toggleOrderButton = (
      <button
        onClick={() => this.toggleOrder()}>
        {toggleOrderButtonText}
      </button>
    )

    const moves = sortedHistory.map((step, move) => {
      const lastMove = step.lastMove;
      const row = lastMove.row;
      const column = lastMove.column;
      const player = lastMove.x ? 'X' : 'O';
      const moveNumber = lastMove.stepNumber;
      const lastMoveDescription = (function () {
        const displayed = (moveNumber === state.stepNumber) ? '>' : '';

        return displayed + '[ R' + row + 'C' + column + ' by ' + player + ' ]';
      })()

      const description = moveNumber
        ? ('go to move #' + moveNumber + ' ' + lastMoveDescription)
        : 'go to game start';

      return (
        <li key={moveNumber}>
          <button onClick={() => this.jumpTo(moveNumber)}>
            {description}
          </button>
        </li>
      );
    })

    const status = (function () {
      if (winner) return 'winner: ' + winner;
      else if (draw) return 'draw';
      else return 'current turn: player ' + (state.xIsNext ? 'X' : 'O');
    })();

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <div>{toggleOrderButton}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);